package com.example.sharedpreference

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var  sharedPreference:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        readData()
    }

    private fun init() {
        sharedPreference = getSharedPreferences("data", MODE_PRIVATE)
        save_bt.setOnClickListener {
            saveData()
        }
        delete_bt.setOnClickListener {
            deleteData()
        }
    }

    private fun saveData() {
        val email = email_et.text.toString()
        val name = name_et.text.toString()
        val lastName = lastName_et.text.toString()
        val age = age_et.text.toString().toInt()
        val address = address_et.text.toString()
        val editor = sharedPreference.edit()
        if (email.isNotEmpty() && name.isNotEmpty() && lastName.isNotEmpty()
             && address.isNotEmpty() && age.toString().isNotEmpty()){
            editor.putString("email", email)
            editor.putString("last_name", lastName)
            editor.putString("name", name)
            editor.putString("age", age.toString())
            editor.putString("address", address)
            editor.apply()
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Please Fill all Field!!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun readData() {
        val email = sharedPreference.getString("email", "")
        val name = sharedPreference.getString("name", "")
        val lastName = sharedPreference.getString("last_name", "")
        val age = sharedPreference.getString("age", "")
        val address = sharedPreference.getString("address", "")
        if (email!!.isEmpty()){
            email_et.setText("")
            name_et.setText("")
            lastName_et.setText("")
            address_et.setText("")
            age_et.setText("")
        }else{
            email_et.setText(email)
            name_et.setText(name)
            lastName_et.setText(lastName)
            address_et.setText(address)
            age_et.setText(age)
            email_et.isEnabled = false
            name_et.isEnabled = false
            lastName_et.isEnabled = false
            age_et.isEnabled = false
            address_et.isEnabled = false

        }


    }
    private fun deleteData(){
        val editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
        email_et.setText("")
        name_et.setText("")
        lastName_et.setText("")
        address_et.setText("")
        age_et.setText("")
        email_et.isEnabled = true
        name_et.isEnabled = true
        lastName_et.isEnabled = true
        age_et.isEnabled = true
        address_et.isEnabled = true
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show()
    }
}